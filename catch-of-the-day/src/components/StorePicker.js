import React from 'react';
import PropTypes from 'prop-types';

import { getFunName } from '../helpers'

class StorePicker extends React.Component
{
    static propTypes = {
        history: PropTypes.object.isRequired
    };

    storeInput = React.createRef();

    baseUrl = '/store/';

    goToStore = (event) => {
        // Stop the form from submitting, allows to handle submit event on client side
        event.preventDefault();
        const storeName = this.storeInput.current.value;
        // Change page without reload
        this.props.history.push(`${this.baseUrl}${storeName}`);
    }

    render() {
        return (
            <form className="store-selector" onSubmit={this.goToStore}>
                <h2>Please Enter A Store</h2>
                <input
                    type="text"
                    required
                    placeholder="Store Name"
                    defaultValue={getFunName()}
                    ref={this.storeInput}
                />
                <button type="submit">Visit Store ➡</button>
            </form>
        );
    }
}

export default StorePicker;