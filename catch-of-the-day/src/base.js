import Rebase from 're-base';
import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyAMUu-P6bgaB7Maz4Gvnm682-MQlu4-gik",
    authDomain: "catch-of-the-day-f6b3a.firebaseapp.com",
    databaseURL: "https://catch-of-the-day-f6b3a.firebaseio.com",
};

const fireBaseApp = firebase.initializeApp(config);

const base = Rebase.createClass(fireBaseApp.database());

export { fireBaseApp };

export default base;